const endpoint_prefix = "http://localhost:3010/products";

const PARAM_LIMIT = "_limit";
const PARAM_TAGS_LIKE = "tags_like";
const PARAM_PRICE_EQ = "price_lte";
const PARAM_SUBSCRIPTION = "subscription";

const getProducts = async (filter, pageLimit = 20) => {
  let params = [];
  if (filter && filter.tag.trim().length > 0) {
    params.push(`${PARAM_TAGS_LIKE}=${filter.tag}`);
  }

  if (filter && filter.price.trim().length > 0) {
    params.push(`${PARAM_PRICE_EQ}=${filter.price}`);
  }

  if (filter && filter.subscription.trim().length > 0) {
    params.push(`${PARAM_SUBSCRIPTION}=${filter.subscription}`);
  }

  params.push(`${PARAM_LIMIT}=${pageLimit}`);

  const response = await fetch(`${endpoint_prefix}?${params.join("&")}`);
  if (!response.ok) {
    throw new Error("Something went wrong!");
  }

  return await response.json();
};

const ProductsApi = {
  getProducts,
};

export default ProductsApi;
