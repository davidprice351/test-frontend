import styles from "./App.module.css";
import ProductsCollection from "./pages/ProductsCollection";

function App() {
  return (
    <div className={styles.app}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <img src="logo.png" alt="PetLabCo Logo" />
        </div>
      </header>
      <ProductsCollection />
    </div>
  );
}

export default App;
