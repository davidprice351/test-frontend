import React, { useState, useCallback, useEffect } from "react";
import styles from "./ProductsCollection.module.css";
import ProductTable from "../components/Product/ProductTable";
import ProductFilter from "./../components/Product/ProductFilter";
import ProductsApi from "../api/ProductsApi";

const ProductsCollection = () => {
  const [products, setProducts] = useState([]);
  const [productFilter, setProductFilter] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetchProductsHandler = useCallback(async (productFilter) => {
    setIsLoading(true);
    setError(null);
    try {
      let products = await ProductsApi.getProducts(productFilter);

      setProducts(products);
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    fetchProductsHandler(productFilter);
  }, [fetchProductsHandler, productFilter]);

  const filterHandler = (productFilter) => {
    setProductFilter(productFilter);
  };

  return (
    <div className={styles.products}>
      <aside className={styles.side}>
        <ProductFilter onFilter={filterHandler} />
      </aside>
      <main className={styles.main}>
        {!isLoading && <ProductTable products={products} />}
        {isLoading && <p>Loading products</p>}
        {error && <p>Whoops an error has occured</p>}
      </main>
    </div>
  );
};

export default ProductsCollection;
