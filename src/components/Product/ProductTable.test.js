import { render, screen } from "@testing-library/react";
import ProductTable from "./ProductTable";

const dummy_product_list = [
  {
    id: 1,
    slug: "product-slug",
    title: "Product title",
    vendor: "vendor",
    tags: ["tag-a", "tag-b"],
    published: true,
    url: "https://thepetlabco.com/products/joint-chews-for-dogs",
    image_src: "url",
    option_value: "options",
    sku: "HHC4",
    price: 20.99,
    subscription_discount: 19,
    subscription: true,
  },
];

const empty_product_list = [];

describe("Product Table", () => {
  test("No Products", () => {
    render(<ProductTable products={empty_product_list} />);
    const productTitle = screen.getByText(/No products found/i);
    expect(productTitle).toBeInTheDocument();
  });

  test("Display Products ", () => {
    render(<ProductTable products={dummy_product_list} />);
    const productPrice = screen.getByText(/Product Title/i);
    expect(productPrice).toBeInTheDocument();
  });
});
