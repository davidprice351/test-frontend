import { render, screen } from "@testing-library/react";
import Product from "./Product";

const dummy_product = {
  id: 1,
  slug: "product-title",
  title: "Product Title",
  vendor: "Vendor",
  tags: ["tag-a", "tag-b"],
  published: true,
  url: "https://thepetlabco.com/products/joint-chews-for-dogs",
  image_src: "url",
  option_value: "options",
  sku: "PRODUCT_SKU",
  price: 99.99,
  subscription_discount: 25,
  subscription: true,
};

describe("Product component", () => {
  test("Product Title is rendered", () => {
    render(<Product product={dummy_product} />);
    const productTitle = screen.getByText(/Product Title/i);
    expect(productTitle).toBeInTheDocument();
  });

  test("Product Price is rendered", () => {
    render(<Product product={dummy_product} />);
    const productPrice = screen.getByText(/99.99/i);
    expect(productPrice).toBeInTheDocument();
  });
});
