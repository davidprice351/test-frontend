import { useState } from "react";
import styles from "./ProductFilter.module.css";

const defaultFilter = {
  tag: "",
  price: "",
  subscription: "",
};

const ProductFilter = (props) => {
  const [productFilter, setProductFilter] = useState(defaultFilter);

  const changeTagHandler = (evt) => {
    setProductFilter((prevState) => {
      return { ...prevState, tag: evt.target.value };
    });
  };

  const changePriceHandler = (evt) => {
    setProductFilter((prevState) => {
      return { ...prevState, price: evt.target.value };
    });
  };

  const changeSubscriptionHandler = (evt) => {
    setProductFilter((prevState) => {
      return { ...prevState, subscription: evt.target.value };
    });
  };

  const submitFormHandler = (evt) => {
    evt.preventDefault();
    props.onFilter(productFilter);
  };

  return (
    <div title="product-filter" className={styles.filter}>
      <h2 className={styles.heading}>Filter Products</h2>
      <form name="product_list" onSubmit={submitFormHandler}>
        <div className={styles.facet}>
          <label htmlFor="tags">Tags</label>
          <input
            id="tags"
            type="text"
            onChange={changeTagHandler}
            value={productFilter.tag}
          />
        </div>
        <div className={styles.facet}>
          <label htmlFor="price">Price</label>
          <input
            id="price"
            type="text"
            onChange={changePriceHandler}
            value={productFilter.price}
          />
        </div>
        <div onChange={changeSubscriptionHandler}>
          <label htmlFor="Subscription">Subscription</label>
          <input type="radio" value="true" name="Subscription" /> Yes
          <input type="radio" value="false" name="Subscription" /> No
        </div>
        <div className={styles.actions}>
          <button className={styles.button} type="submit">
            Search
          </button>
        </div>
      </form>
    </div>
  );
};

export default ProductFilter;
