import styles from "./Product.module.css";

const Product = (props) => {
  const product = props.product;
  return (
    <div className={styles.product}>
      <div className={styles.details}>
        <h2 className={styles.title}>{product.title}</h2>
        <div className={styles.tags}>
          {product.tags.map((tag, idx) => (
            <span key={`${product.id}${tag}${idx}`} className={styles.tag}>
              {tag}
            </span>
          ))}
        </div>
        <p>{product.vendor}</p>
        <p className={styles.price}>£{product.price.toFixed(2)}</p>
      </div>
      <div className={styles.image}>
        <img src={product.image_src} alt={product.title} />
      </div>
    </div>
  );
};

export default Product;
