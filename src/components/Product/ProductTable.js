import React, { useState, useMemo } from "react";
import Product from "./Product";
import styles from "./ProductTable.module.css";
import Pagination from "../Pagination/Pagination";

const ProductList = (props) => {
  let PageSize = 5;
  const [currentPage, setCurrentPage] = useState(1);

  const currentTableData = useMemo(() => {
    if (props.products.length > 0) {
      const firstPageIndex = (currentPage - 1) * PageSize;
      const lastPageIndex = firstPageIndex + PageSize;
      return props.products.slice(firstPageIndex, lastPageIndex);
    }
  }, [currentPage]);

  if (props.products.length > 0) {
    return (
      <>
        <div className={styles.productheading}>
          Product Count: {props.products.length}
        </div>

        <ul title="product-table" className={styles.products}>
          {props.products.map((product) => {
            return (
              <li className={styles.items} key={product.id}>
                <Product product={product} />
              </li>
            );
          })}
        </ul>

        <Pagination
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={props.products.length}
          pageSize={PageSize}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </>
    );
  }

  return <p>No products found</p>;
};

export default ProductList;
